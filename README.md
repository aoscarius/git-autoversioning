# GIT Autoversioning Hook Tools

*authors*: Oscar Castello aka A[O]scar(ius)

*license*: Creative Commons Attribution-ShareAlike 4.0 International License.

This tool is a series of hook scripts for the git versioning system to permit 
the automatic generation of new version number at every commit and automatic
generation of changelog and tag system.
The generation of the version number can will be controlled trought a special 
syntax format system of the commit message. 

## Commit parser system

Format of commit message:
```
git commit -F- << EOF
    [<build>] The text of commit with small description of it

    (<feature>) Description of feature one
    (<feature>) Description of feature two
    ...
    (<feature>) Description of feature n
EOF
```

The possible parameters are:

```
build = {
    a   | alpha             ->  set the build number to 0
    b   | beta              ->  set the build number to 1
    rc  | release-candidate ->  set the build number to 2
    r   | release           ->  set the build number to 3
    new | major             ->  increment the major number and set all to 0
}
```

```
feature = {
    +   ->  added feature (increment the minor number)
    -   ->  removed feature (increment only the maintenance number)
    f   ->  fixed a bug (increment only the maintenance number)
    c   ->  changed a function (increment only the maintenance number)
}
```

Example of commit message:
```
git commit -F- << EOF
    [alpha] This release correct the previous version bugs and improve the performance of the system

    (+) Added the support to new file syntax
    (+) Added the new log system
    ...
    (f) Fixed the bug in the main module
EOF
```

## Versioning system

The version number scheme that this tool use is:

    v(major.minor[.maintenance[.build]][-revision])
    
The build position possibile values are:

    0 for alpha (status)
    1 for beta (status)
    2 for release candidate
    3 for (final) release
    
For instance:

    v1.2.0.0-3 instead of 1.2-a and this is the third revision of the system
    v1.2.1.1-11 instead of 1.2-b (beta with some bug fixes)
    v1.2.2.2-24 instead of 1.2-rc (release candidate with some bug fixes)
    v1.2.0.3-18 instead of 1.2-r (commercial distribution)

The number is generated in combination with last tag name, formatted as example "v0.0.0a". So you can set the start number with 

    git tag v0.0.0a 

where the 0.0.0a is your first version number. Otherwise the system set the first automatically.

## Tagging system

Don't forget to push the tag to remote repository: 

    git push --tags

## Changelog system

The changelog is generated and updated in the markdown format.

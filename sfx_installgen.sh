#!/bin/bash

# GIT Autoversioning Hook Tools
#
# @filename:      
#   sfx_installgen.sh
#
# @description:
#   This script generate a selfextract installer of the tool.
#
# @authors: 
#   Oscar Castello aka A[O]scar(ius)
#
# @license:
#   Creative Commons Attribution-ShareAlike 4.0 International License.

VERSION="0.4"
SFXNAME="git-versioning.sfx"

# Generate autoextract installer file
cat << _EOF_ > "$SFXNAME"
#!/bin/bash

VERSION="0.4"

echo
echo "--== GIT AUTOVERSIONG HOOK TOOLS v\$VERSION ==--"
echo
echo "Now the system install hooks on your repository."
echo "After this, your future commit will done with automatic generation of tagging, versioning and changelog"
echo 

export TMPDIR=\$(mktemp -d "/tmp/\$0.XXXXX")
ARCHIVE=\$(awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' \$0)

tail -n+\$ARCHIVE \$0 | tar -xzf - -C "\$TMPDIR"

CDIR=\$(pwd)
cp "\$TMPDIR/commit-msg"            "\$CDIR/.git/hooks/commit-msg" 	&& chmod +x "\$CDIR/.git/hooks/commit-msg"
cp "\$TMPDIR/update-script" 	    "\$CDIR/update-script" 			&& chmod +x "\$CDIR/update-script"
cp "\$TMPDIR/post-commit" 	        "\$CDIR/.git/hooks/post-commit"	&& chmod +x "\$CDIR/.git/hooks/post-commit"
cp "\$TMPDIR/git-autoversioning.md"	"\$CDIR/git-autoversioning.md"
rm -rf "\$CDIR/\$0" "\$TMPDIR"

echo "INSTALLATION COMPLETED"
echo

exit 0

__ARCHIVE_BELOW__
_EOF_

# Do executable
chmod +x $SFXNAME

# Generate the payload
cp "README.md" "script/git-autoversioning.md"
tar -czf payload.tar.gz -C script .
rm -f "script/git-autoversioning.md"

# Attach the payload
if [ -e "payload.tar.gz" ]; then
    cat payload.tar.gz >> $SFXNAME
    rm payload.tar.gz
else
    echo "payload.tar.gz does not exist"
    exit 1
fi

echo "Self extractiong archive $SFXNAME successfull created. Copy this file in your git repo and execute it to install the hooks."
exit 0

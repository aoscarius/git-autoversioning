
## [v0.4.0.2rc-1] - 29/09/2018
This is the first release candidate of the autoversioning system hook for git. Fix same bug discovered during the test and add some small feature.
### Added
- Add the support for the automatic versioning
- Add the support for the automatic tagging
- Add the support for the automatic changelog
- Add the control of the first tag if it's a already exist repository
### Fixed
- Fixed a bug in the regex string used to parse the commit message
- Fix a bug in the versioning regex that generate recursively matching
- Fix in the local update-script
- Fix in the versioning number incrementation system
### Changed
- Small change in the README.md, correct the tagging section

